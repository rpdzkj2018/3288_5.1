#
# Copyright 2014 The Android Open-Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

PRODUCT_PACKAGES += \
    Launcher3

#$_rbox_$_modify_$_zhengyang: add displayd
PRODUCT_PACKAGES += \
    displayd

#enable this for support f2fs with data partion
#BOARD_USERDATAIMAGE_FILE_SYSTEM_TYPE := f2fs
# This ensures the needed build tools are available.
# TODO: make non-linux builds happy with external/f2fs-tool; system/extras/f2fs_utils
#ifeq ($(HOST_OS),linux)
#TARGET_USERIMAGES_USE_F2FS := true
#endif

ifeq ($(strip $(TARGET_BOARD_PLATFORM_PRODUCT)), box)
BOARD_SEPOLICY_DIRS += \
      device/rockchip/rk3288/rk3288_box/sepolicy
BOARD_SEPOLICY_UNION += \
      service_contexts
PRODUCT_COPY_FILES += \
    device/rockchip/rk3288/rk3288_box/init.rc:root/init.rc \
    device/rockchip/rk3288/fstab.rk30board.bootmode.unknown:root/fstab.rk30board.bootmode.unknown \
    device/rockchip/rk3288/rk3288_box/fstab.rk30board.bootmode.emmc:root/fstab.rk30board.bootmode.emmc
else
  PRODUCT_COPY_FILES += \
    device/rockchip/rk3288/init.rc:root/init.rc \
    device/rockchip/rk3288/fstab.rk30board.bootmode.unknown:root/fstab.rk30board.bootmode.unknown \
    device/rockchip/rk3288/fstab.rk30board.bootmode.emmc:root/fstab.rk30board.bootmode.emmc
endif


# 荣品电子系统定制控制

# 导航栏
BOARD_HAS_NAV := true

#状态栏
BOARD_HAS_STATUSBAR := true

#中文
BOARD_HAS_CHN := false



#原始launcher
BOARD_HAS_LAUNCHER 	:= true

#超级用户
BOARD_SUPERUSER := false

#USB权限弹窗申请
BOARD_NO_USB_PERMISSION := true


#dpi设置 数值越小，图标越小，布局区域越大；反之，则相反
BOARD_DPI := 160

# 系统默认旋转角度 0 90 180 270
BOARD_HW_ROTATION := 0

#camera rotation 0 90 180 270
BOARD_BACK_CAMERA_ROTATION := 0
BOARD_FRONT_CAMERA_ROTATION := 0

# 默认USB摄像头为前摄像头
BOARD_USBCAMERA_ONLY_FRONT := 0

# 默认USB摄像头为前摄像头
BOARD_USBCAMERA_ONLY_BACK := 0
# 修改以上选项后，先执行make installclean ，再编译android。对编译不熟悉的，直接删除out目录，再进行全局编译。
# end






# Hidden navigation bar	
ifeq ($(BOARD_HAS_NAV),false)
PRODUCT_PROPERTY_OVERRIDES += \
	qemu.hw.mainkeys=1
endif


# Hidden status bar	
ifeq ($(BOARD_HAS_STATUSBAR),false)
PRODUCT_PROPERTY_OVERRIDES += \
	ro.rpdzkj.no.statusbar=true
endif

ifeq ($(BOARD_HAS_CHN),true)	
PRODUCT_PROPERTY_OVERRIDES += \
	persist.sys.timezone=Asia/Shanghai \
	ro.product.locale.language=ZH \
	persist.sys.language=ZH \
	persist.sys.country=CN \
	ro.product.locale.region=CN
endif



#superuser
ifeq ($(strip $(BOARD_SUPERUSER)),true)
PRODUCT_COPY_FILES += \
	vendor/rockchip/rpdzkj/superuser/lib/libdummy.so:system/lib/libdummy.so \
	vendor/rockchip/rpdzkj/superuser/xbin/daemonsu:system/xbin/daemonsu \
	vendor/rockchip/rpdzkj/superuser/app/Superuser.apk:system/priv-app/Superuser.apk \
	vendor/rockchip/rpdzkj/superuser/xbin/su:system/xbin/su \
	vendor/rockchip/rpdzkj/superuser/bin/install-recovery.sh:system/bin/install-recovery.sh
endif

#	vendor/rockchip/rpdzkj/superuser/app/Superuser.apk:system/priv-app/Superuser.apk \
## 如果使用superuser.apk,应用申请超级用户权限的时候会弹出来窗口，等待用户允许或禁止，如果不加入superuser.apk,系统会自动同意	


ifeq ($(BOARD_NO_USB_PERMISSION),true)	
PRODUCT_PROPERTY_OVERRIDES += \
	ro.rpdzkj.no.usb.permission=true
endif


# ro.sf.hwrotation
PRODUCT_PROPERTY_OVERRIDES += \
	ro.sf.hwrotation=$(BOARD_HW_ROTATION)

# dpi
PRODUCT_PROPERTY_OVERRIDES += \
	ro.sf.lcd_density=$(BOARD_DPI)
	

# camera rotation
PRODUCT_PROPERTY_OVERRIDES += \
	ro.sf.back.camera.rotation=$(BOARD_BACK_CAMERA_ROTATION) \
	ro.sf.front.camera.rotation=$(BOARD_FRONT_CAMERA_ROTATION)
# uvc camera only front
PRODUCT_PROPERTY_OVERRIDES += \
	ro.sf.only.front=$(BOARD_USBCAMERA_ONLY_FRONT)
# uvc camera only back
PRODUCT_PROPERTY_OVERRIDES += \
	ro.sf.only.back=$(BOARD_USBCAMERA_ONLY_BACK)




# setup dalvik vm configs.
$(call inherit-product, frameworks/native/build/tablet-10in-xhdpi-2048-dalvik-heap.mk)


$(call inherit-product-if-exists, vendor/rockchip/rk3288/device-vendor.mk)

$(call inherit-product-if-exists, vendor/rockchip/rpdzkj/rpdzkj.mk)


